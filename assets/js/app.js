var game = new Phaser.Game(800, 600, Phaser.AUTO, 'knitpat', { preload: preload, create: create, update: update, render: render });

function preload() {
    game.load.image('tiles', 'assets/img/symbols.png');
}

var map;
var layer1;

var marker;
var currentTile = 0;
var layer1;

var cursors;
var showLayersKey;

let rows = 40;
let cols = 30;
let tilesize = 32;

function create() {

    game.stage.backgroundColor = '#2d2d2d';

    //  Creates a blank tilemap
    map = game.add.tilemap();

    //  Add a Tileset image to the map
    map.addTilesetImage('tiles');

    //  Creates a new blank layer and sets the map dimensions.
    layer1 = map.create('level1', rows, cols, tilesize, tilesize);
    layer1.scrollFactorX = 0.5;
    layer1.scrollFactorY = 0.5;
    layer1.resizeWorld();

    //  Create our tile selector at the top of the screen
    createTileSelector();

    // blank canvas
    for (let i = 0; i < rows; i++) {
        for (let j = 0; j < cols; j++) {
            map.putTile(currentTile, i, j, layer1);
        }
    }

    game.input.addMoveCallback(updateMarker, this);

    cursors = game.input.keyboard.createCursorKeys();
}

function pickTile(sprite, pointer) {
    currentTile = game.math.snapToFloor(pointer.x, tilesize) / tilesize;
}

function updateMarker() {

    marker.x = layer1.getTileX(game.input.activePointer.worldX) * tilesize;
    marker.y = layer1.getTileY(game.input.activePointer.worldY) * tilesize;

    if (game.input.mousePointer.isDown)
    {
        // console.log(currentTile, layer1.getTileX(marker.x), layer1.getTileY(marker.y), layer1)
        map.putTile(currentTile, layer1.getTileX(marker.x), layer1.getTileY(marker.y), layer1);
    }

}

function update() {
}

function render() {

    // game.debug.text('Current Layer: ' + layer1.name, 16, 550);
    // game.debug.text('1-3 Switch Layers. SPACE = Show All. Cursors = Move Camera', 16, 570);

}

function createTileSelector() {

    //  Our tile selection window
    var tileSelector = game.add.group();

    var tileSelectorBackground = game.make.graphics();
    tileSelectorBackground.beginFill(0x000000, 0.5);
    tileSelectorBackground.drawRect(0, 1, tilesize * cols + 2, tilesize + 2);
    tileSelectorBackground.endFill();

    tileSelector.add(tileSelectorBackground);

    var tileStrip = tileSelector.create(1, 1, 'tiles');
    tileStrip.inputEnabled = true;
    tileStrip.events.onInputDown.add(pickTile, this);

    tileSelector.fixedToCamera = true;

    //  Our painting marker
    marker = game.add.graphics();
    marker.lineStyle(2, 0x00FF00, 1);
    marker.drawRect(0, 0, tilesize, tilesize);

}